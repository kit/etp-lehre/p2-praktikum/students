# Technische Daten und Inventar für den Versuch **Spezifische Wäremkapazität**

Für die verschiedenen Aufgaben des Versuchs **Spezifisch Wärmekapazität** stehen Ihnen die folgenden Geräte und Materialien zur Verfügung: 

- Ein [Dewargefäß](https://de.wikipedia.org/wiki/Dewargef%C3%A4%C3%9F) aus Glas ($V=250\ \mathrm{cm}^{3}$) mit einem Plexiglasdeckel mit Einfüllöffnung und Trichter zur Verwendung als Kalorimeter.
- Ein Halbleiterthermometer mit Digitalanzeige (in Schritten von $0.1^{\circ}\mathrm{C}$).
- Ein Quecksilberthermometer mit einem Messbereich von $-3$ bis $+50^{\circ}\mathrm{C}$ (mit Skaleneinteilungen von $0.2^{\circ}\mathrm{C}$). 
- Ein elektrischer Heisswasserbereiter ($V=300\ \mathrm{cm}^{3}$). 
- Eine Präzisionswaage. 
- Diverse Aluminium-, Messing- und Kupferzylinder, sowie Aluminium-, Blei-, Zinn- und Kupfergranulat. 
- Verschiedene weitere Messzylinder, [Erlenmeyerkolben](https://de.wikipedia.org/wiki/Erlenmeyerkolben), Bechergläser und Dewargefäße.
- Ein Aluminium-Hohlzylinder ($m_{\mathrm{Al}}=338\ \mathrm{g}$) mit eingebauter Heizwicklung ($I_{\mathrm{max}}=2\ \mathrm{A}$) und einem eingebauten $\mathrm{NiCr}$-$\mathrm{Ni}$-[Thermoelement](https://de.wikipedia.org/wiki/Thermoelement). Die Thermospannung als Funktion der Temperatur finden Sie im Verzeichnis *params* in diesem gitlab-Repository (Dateiname: [calibration.csv](https://gitlab.kit.edu/kit/etp-lehre/p2-praktikum/students/-/blob/main/Spezifische_Waermekapazitaet/params/calibration.csv)). Die Stromversorgung für den Heizdraht liefert bis zu $5\ \mathrm{A}$ bei max. $16\ \mathrm{V}$.
- Ein Edelstahl- und ein Nalgene-Isolierbehälter.
- Ein Computer mit Picoscope TC08, als Datenlogger.
- Schutzbrillen, Schutzhandschuhe und Pinzetten.

Die folgenden **Referenzwerte für $c_{\mathrm{M}}$** können Sie ohne weitere Angabe von Quellen und ohne Angabe weiterer Unsicherheiten in Ihrer Auswertung verwenden: 

  * Aluminium (Erwartung: $c_{\mathrm{Al}}=896\, \mathrm{J\,kg^{-1}\,K^{-1}}$);
 * Kupfer (Erwartung $c_{\mathrm{Cu}}=385\, \mathrm{J\,kg^{-1}\,K^{-1}}$); 
 * Messing (Erwartung $c_{\mathrm{CuZn}}=376\, \mathrm{J\,kg^{-1}\,K^{-1}}$); 
 * Blei (Erwartung $c_{\mathrm{Pb}}=131\, \mathrm{J\,kg^{-1}\,K^{-1}}$); und 
 * Zinn (Erwartung $c_{\mathrm{Sn}}=221\, \mathrm{J\,kg^{-1}\,K^{-1}}$).

Die Werte sind jeweils für $\vartheta=20^{\circ}\mathrm{C}$ gegeben.
