# Hinweise für den Versuch **Spezifische Wärmekapazität**

## Spezifische Wärmekapazität als Funktion der Temperatur

Für diese Messung kühlen Sie einen Aluminium-Hohlzylinder (AL) in einem Wärmebad von Flüssigstickstoff auf möglichst tiefe Temperaturen ab. Unter Normaldruck beträgt die Siedetemperatur von Stickstoff $77\ \mathrm{K}\ (-196^{\circ}\mathrm{C})$. Daraufhin führen Sie dem AL durch eine Heizspule mit der Leistung $P_{\mathrm{Heiz}}$ in festen Zeitintervallen $\mathrm{dt}$ kontrolliert Wärme $\delta Q = P_{\mathrm{Heiz}}\,\mathrm{d}t$ zu. Da dabei keine Arbeit verrichtet wird ($\delta W=0$) geht $\delta Q$ vollständig in innere Energie ($\mathrm{d}U$) über:

$$
\begin{equation*}
P_{\mathrm{H}}\,\mathrm{d}t = \delta Q = \mathrm{d}U = c_{\mathrm{Al}}(T)\,m_{\mathrm{Al}}\,\mathrm{d} T.
\end{equation*}
$$
Daraus ergibt sich 

$$
\begin{equation}
\begin{split}
&c_{\mathrm{Al}}(T) = \frac{P_{\mathrm{Heiz}}}{m_{\mathrm{Al}}\, \dot{T}(T)};\\ 
&\\
&\text{mit}\\
&\\
&\dot{T}(T) = \lim\limits_{\Delta t\to0}\left(\frac{\Delta T}{\Delta t}\right).\\ 
\end{split}
\end{equation}
$$

### Messprinzip

Für diese Messung sind $m_{\mathrm{Al}}$ und $P_{\mathrm{Heiz}}$ durch den experimentellen Aufbau fest vorgegeben. Zu messen ist der Verlauf $\dot{T}(T)$. 

Die Daten hierzu nehmen Sie, mit Hilfe eines Datenloggers, automatisiert auf. Es handelt sich um Spannungen eines $\mathrm{NiCr}$-$\mathrm{Ni}$-Thermoelements (TE, in $\mathrm{V}$) als Funktion der Zeit (in $\mathrm{s}$), in der der Datenlogger die Werte ausließt. 

Aus diesen Daten den Verlauf von $c_{\mathrm{Al}}(T)$ zu extrahieren ist eine Übung der **Datenanalyse**, die wir im Rahmen dieser Aufgabe in vier Schritten etwas weiter ausführen werden.

Protokollieren Sie Ihr Vorgehen so, dass Sie später in **jedem einzelnen Schritt** eine klare Motivation Ihres Handelns, die Schwierigkeiten und wichtigsten Punkte, auf die bei der Bearbeitung der jeweiligen Teilaufgabe zu achten ist, ggf. das Ergebnis der Aufgabe und eine Einschätzung dazu ableiten können. 

### Beschreibung der Rohdaten

Damit Sie und andere, später maximal vom Dokument Ihrer Arbeit profitieren können, stellen Sie Ihrer Auswertung eine hinreichende Beschreibung der Rohdaten voran. Hierzu sollten Sie Ihrem Protokoll nicht nur eine Beschreibung Ihres Vorgehens (**Aufgabe 2.1**), sondern auch eine Beschreibung der Funktionsweise des TE (**Aufgabe 2.2**) zufügen.

### Kalibration

Zur Kalibration der Daten (**Aufgabe 2.2**) benötigen Sie Messpunkte $(U_{i}, T_{i})$, die die gemessenen Spannungen ($U_{i}$) des TE mit Temperaturen ($T_{i}$) verknüpfen. Wir stellen Ihnen  eine solche Messung in der Datei [calibration.csv](https://gitlab.kit.edu/kit/etp-lehre/p2-praktikum/students/-/blob/main/Spezifische_Waermekapazitaet/params/calibration.csv) bereit. 

Die zu extrahierende **Kalibrationskurve bezeichnen wir als $K(U, T)$**. Eine durch ein Modell der physikalischen Vorgänge motivierte Parametrisierung kann nützlich sein, ist aber in diesem Fall nicht zwingend, da Sie ohnehin schwer alle eine solche Messung beeinflussenden äußeren Parameter kontrollieren können. Trotzdem sollten Sie auf **relative Einfachheit der funktionalen Form** achten.  

Für Ihr weiteres Vorgehen ist eine Einschätzung der Güte des Modells für $K(U,T)$, z.B. basierend auf dem $\chi^{2}$-Wert der Anpassung unumgänglich. Sie sollten darauf achten, dass $K(U, T)$ die Datenpunkte nach Anpassung wirklich gut beschreibt, denn diese Kalibrationskurve bildet die Grundlage für alle weiteren Schritte. Hierzu ist eine seriöse Angabe der Unsicherheiten $\Delta U_{i}$ und $\Delta T_{i}$ unerlässlich. 

### Korrektur des Wärmegangs

Wenn der AL zusätzlich zur kontrollierten elektrischen Wärmezufuhr Wärme aus der Umgebung aufnimmt führt dies offensichtlich zu einer Verzerrung der Daten. Sie können diesen Effekt mit Hilfe einer Leermessung ohne elektrische Wärmezufuhr abschätzen und die originale Messung a posteriori (d.h. nachdem die Messung bereits beendet ist) entsprechend korrigieren. Wir haben eine solche Leermessung, in der Datei [waermegang.csv](https://gitlab.kit.edu/kit/etp-lehre/p2-praktikum/students/-/blob/main/Spezifische_Waermekapazitaet/params/waermegang.csv), für Sie bereitgestellt. 

Ob Sie die Korrektur des Wärmegangs auf den Rohdaten des TE (in $\mathrm{V}$) oder auf den kalibierten Daten (in $\mathrm{K}$) durchführen scheint zunächst gleich. Tatsächlich macht es mehr Sinn, den Wärmegang erst zu kalibieren und dann als $T_{0}(t)$ zu bestimmen. Beachten Sie, dass $K(U,T)$ i.a. keine Gerade ist! Durch die Korrektur auf $T_{0}(t)$ sollte es Ihnen möglich sein den Wärmegang durch eine einfache und physikalisch motivierte funktionale Form zu beschreiben, die Sie in den weiteren Aufgabenteilen leicht weiter verwenden können. 

In der Vergangenheit hat sich ein einfaches Modell der Form 

$$
\begin{equation}
T_{0}(t)=\alpha+\beta\,e^{t/\gamma}
\end{equation}
$$
bewährt, wobei $\alpha$, $\beta$ und $\gamma$ freie Parameter des Modells sind.

Auch für diese Aufgabe ist eine Diskussion der Güte des Modells, z.B. basierend auf dem $\chi^{2}$-Wert der Anpassung an die Daten unumgänglich.

### Bestimmung von $\dot{T}(T)$

Die Bestimmung von $\dot{T}(T)$ erfolgt auf den Daten die alle bisherigen Korrekturen durchlaufen haben.    

Bestimmen Sie hierzu zunächst durch Anpassung eine analytische Funktion zur Beschreibung des korrigierten Verlaufs von $T(t)$. Hierzu eignet sich ein Modell der Form

$$
\begin{equation}
T(t) = a\,t^{b} + c,
\end{equation}
$$
mit den freien Parametern $a$, $b$ und $c$. 

Aus dem Modell können Sie die Ableitung wie folgt leicht bestimmen: 

$$
\begin{equation*}
\dot{T}(t) = a\,b\,t^{b-1}.
\end{equation*}
$$
Um $\dot{T}$ als Funktion von $T$ zu bestimmen benötigen Sie noch die Umkehrfunktion von Gleichung **(3)** 

$$
\begin{equation*}
t(T) = \left(\frac{T-c}{a}\right)^{1/b}.
\end{equation*}
$$
Nach Einsetzen ergibt sich eine analytische Abschätzung des gesuchten Zusammenhangs zu 
$$
\begin{equation*}
\dot{T}(T) = a\, b\, \left(\frac{T-c}{a}\right)^{\frac{b-1}{b}}.
\end{equation*}
$$
Auch hier ist es wieder essentiell sehr gewissenhaft, z.B. basierend auf dem $\chi^{2}$-Wert der Anpassung, zu kontrollieren, wie gut das Modell aus Gleichung **(3)** die Datenpunkte beschrieben kann. Nur in dem Rahmen, in dem dies gewährleistet ist ergibt der Verlauf von $c_{\mathrm{Al}}(T)$ einen Sinn.   

# Navigation

[Main](https://gitlab.kit.edu/kit/etp-lehre/p2-praktikum/students/-/tree/main/Spezifische_Waermekapazitaet)
