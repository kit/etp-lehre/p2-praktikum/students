# Hinweise für den Versuch Operationsverstärker

## Versuchsdurchführung

### Steckbrett

Alle Schaltungen werden auf dem in **Abbildung 24** gezeigten, vorbereiteten Steckbrett (*breed board*) aufgebaut. 

<img src="../figures/Steckbrett.png" width="1000" style="zoom:100%;" />

**Abbildung 24**: (Steckbrett zum Aufbau der für den Versuch verwendeten Schaltungen)

---

Dieses besteht aus vier Bereichen, die für verschiedene Aufgabenteile verwendet werden:

- **(1) Transistor-Bereich**: Dieser Bereich ist dazu vorgesehen die Emitterschaltung aus **Aufgabe 1** aufzubauen.
- **(2) OPVs 1 und 2**: In diesen Bereichen werden die OPV-Schaltungen für die **Aufgaben 2 bis 4.2** aufgebaut. Links und rechts in diesem Bereich beﬁndet sich jeweils ein bereits an die Spannungsversorgung angeschlossener OPV. In der Mitte befindet ein $10\ \mathrm{k\Omega}$ Potentiometer.
- **(3) OPV 3**: Der dritte OPV in diesem Bereich ist noch nicht an die Spannungsversorgung angeschlossen. Falls, Sie Ihn, wie z.B. für **Aufgabe 4.3** in Gebrauch nehmen möchten müssen Sie die Spannungsversorgung selbst vornehmen.
- **(4) Spannungsversorgung**: Hier ist das Netzteil untergebracht, das die OPVs mit einer Spannung von $\pm15\ \mathrm{V}$ versorgt (blau: $-15\ \mathrm{V}$, rot: $+15\ \mathrm{V}$, schwarz: $0\ \mathrm{V}$ / GND).

Sie müssen die Spannungsversorgung für das Steckbrett vor **Gebrauch anschalten**. Der Schalter befindet sich in Bereich 4. 

### Multimeter

Kalibireren Sie die Angaben des Potentiometers am Steckbrett mit Hilfe des am Versuchsplatz befindlichen Multimeters auf $\Omega$. **Schließen Sie hierzu das Potentiometer vom übrigen Schaltkreis ab**, so dass die Messung unverfälscht ist. Sie messen ansonsten den Widerstand des Netzwerks, von dem das Potentiometer nur ein Teil ist und nicht den Innenwiderstand des Potentiometers. 

Das Multimeter gibt Wechselspannungen als RMS-Amplituden wieder. Der Zusammenhang verschiedener Amplituden-Angaben ist in **Abbildung 25** gezeigt:

<img src="../figures/Amplitudenangaben.png" width="650" style="zoom:100%;" />

**Abbildung 25**: (Zusammenhang verschiedener Spannungsangaben)

---

## Einbinden von externen Bildern ins Protokoll

Wie Sie externe Bilder in Ihr Protokoll einbinden können finden Sie in den folgenden Dokumenten erklärt: 

- [JupyterServer.md](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/blob/main/doc/JupyterServer.md) und darin verlinkt
- [add_figures.ipynb](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students/-/blob/main/tools/add_figures.ipynb).

Elektrische Schaltpläne werden i.a. von links nach recht gelesen. Das Steckbrett ist gegenüber dieser üblichen Lesrichtung gespiegelt. **Gespiegelte Schaltpläne für alle Schaltungen, die für diesen Versuch aufzubauen sind finden Sie im Verzeichnis [*figures/mirrored*](https://gitlab.kit.edu/kit/etp-lehre/p2-praktikum/students/-/tree/main/Operationsverstaerker/figures/mirrored) des Repositories.**

## Aufgabe 1: Emitterschaltung eines Transistors

Achten Sie bei dieser Aufgabe auf das Ausgangssignal des TV. die Eingangssignale sollten klein gewählt sein. Gerade mit der gleichstromgegengekoppelten Emitterschaltung lassen sich Verstärkungen von $v_{U}\gtrsim 120$ erreichen. Die Schaltung liefert aber kein Ausgangssignal jenseits der Versorgungsspannung. Steigt das Ausgangssignal über $12\ \mathrm{V}$ saturiert und die Verstärkung der Schaltung und wird abgeschnitten. 

### Aufgabe 1.1 & 1.2 Einstufiger (gleich)stromrückgekoppelter Transistorverstärker

- Die Angabe von Spannungen mit dem Vermerk $\mathrm{V_{SS}}$ bedeutet "von Spitze zu Spitze [des Signals]" und wird oft, der einfacheren Ablesbarkeit wegen, zur Angabe von periodischen Signalen verwendet.  
- Diskutieren Sie bei Ihrer Beurteilung der TV-Schaltungen, die folgenden Gesichtspunkte: 
  - Die Größe der Verstärkung; 
  - die sich angedeutete Frequenzabhängigkeit der Verstärkung; sowie 
  - die Signalform nach Verstärkung, die ein Maß für die Linearität des Verstärkers ist.  

### Aufgabe 1.3: Frequenzabhängigkeit der Verstärkung

- Sie sollten für jede Schaltung eine Auftragung mit logarithmischer $x(=\nu)$- und linearer $y(=v_{U})$-Achse vornehmen. 
- Beachten Sie bei der Beurteilung, dass die Verstärkung als Funktion von $\nu$ im Fall des gleichstromgegengekoppelten TV um eine Größenordnung variiert. 
- Zusätzlich zu den einfach logarithmischen Auftragungen könnte es sich lohnen beide Messreihen in ein gemeinsames Diagramm mit doppelt logarithmischer Auftragung einzutragen. Dabei würden Sie erwarten, dass sich beide Schaltungen für niedrige Frequenzen gleich verhalten. Die Verstärkung für die Schaltung des stromgegengekoppelten TV knickt ab einer gewissen Grenzfrequenz ab, während die Verstärkung des gleichstromgegengekoppelten TV weiter ansteigt. Im Gegenzug hängt die Verstärkung für den stromgegengekoppelten TV nicht mehr weiter von $\nu$ ab.    

## Aufgabe 2: Nicht-invertierender Spannungsverstärker mit Hilfe eines Operationsverstärkers

### Aufgabe 2.2: Ein- und Ausgangswiderstand

- Zur Kalibration der Angaben des Potentiometers steht Ihnen ein separates Multimeter zur Verfügung. 

## Aufgabe 3: Grundschaltungen für invertierende Verstärker mit Hilfe eines Operationsverstärkers

### Aufgabe 3.2: Aufbau eines Addierers

 * Als Eingangssignale können Sie eine **Drei-, Rechteck- oder Sinusspannung** (mit $\nu\lesssim1\,\mathrm{kHz}$) und eine mit den auf der Platine vorhandenen Potentiometern realisierbare regelbare Gleichspannung im Bereich $-15\ \mathrm{V}$ bis $+15\ \mathrm{V}$ verwenden. 
 * Schalten Sie für diese Messung den **Eingang des Oszilloskops auf „DC-Kopplung“**, damit die Gleichspannungsanteil nicht aus dem Signal ausgefiltert werden!

### Aufgabe 3.3: Aufbau eines Integrierers

 * Als Eingangssignale eignen sich Recht- und Dreieckspannungen niedriger Frequenz (z.B. im Bereich $\nu=50$ bis $100\,\mathrm{Hz}$) und großer Amplitude.
 * Schalten Sie für diese Messung den Eingang des Oszilloskops  wieder zurück auf „AC-Kopplung“!

### Aufgabe 3.4: Aufbau eines Differenzierers

- Als Eingangssignale eignen sich Recht- und Dreieckspannungen niedriger Frequenz (z.B. im Bereich $\nu=50$ bis $500\,\mathrm{Hz}$).

## Aufgabe 4: Komplexere Schaltungen mit Operationsverstärkern

### Aufgabe 4.1: Aufbau eines idealen Einweggleichrichters

* Die Durchlass-Richtung der Dioden ist durch die silberne Umrandung angezeigt.  
* Sie können für diese Aufgabe verschiedene Wechselspannungssignale mit $\nu\lesssim1\,\mathrm{kHz}$ verwenden.
* Machen Sie oszilloskopische Aufnahmen für alle Signale auf der rechten Seite von **Abbildung 21** ([hier](https://gitlab.kit.edu/kit/etp-lehre/p2-praktikum/students/-/blob/main/Operationsverstaerker/doc/Hinweise-OPV.md)). 

# Navigation

[Main](https://gitlab.kit.edu/kit/etp-lehre/p2-praktikum/students/-/tree/main/Operationsverstaerker)

