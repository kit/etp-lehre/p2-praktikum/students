# Hinweise für den Versuch **Operationsverstärker**

## Innenbeschaltung

Die Innenbeschaltung eines OPV ist i.a. sehr komplex und wird in der **Schaltungstheorie der Elektrotechnik** behandelt. Sie kann grundsätzlich immer in drei Abschnitte unterteilt werden:

- Eingangsstufe, 
- Verstärkerstufe, 
- Ausgangsstufe. 

Es ist im Rahmen des Praktikums nicht notwendig die Innenbeschaltung des $\mathrm{\mu A741}$ restlos zu verstehen. Stattdessen werden wir hier die wichtigsten Konzepte beispielhaft erklären. 

#### Eingangsstufe

Die **Eingangsstufe** des $\mathrm{\mu A741}$ ist in **Abbildung 1** gezeigt: 

---

<img src="../figures/OPV_Eingangsstufe.png" width="450" style="zoom:100%;"/>

(**Abbildung 1**: Eingangsstufe des $\mathrm{\mu A741}$)

---

Es handelt sich dabei um einen [**Differenzverstärker**](https://de.wikipedia.org/wiki/Differenzverstärker), an dem die beiden Eingangssignale mit den Spannungen $U^{-}$ (an Pol 2) und $U^{+}$ (an Pol 3) anliegen. Diese liefern die Basisspannungen für zwei baugleiche npn-Transistoren, die durch die Spannungen $V_{S-},\ V_{S+}$ (an den Polen 4 und 7 in **Abbildung 1** [hier](https://gitlab.kit.edu/kit/etp-lehre/p2-praktikum/students/-/tree/main/Operationsverstaerker)) versorgt werden. Die beiden Emitter E2 und E3 sind durch zwei (durch rote Umrandungen gekennzeichnete) **Stromspiegel** gekoppelt. Als [Stromspiegel](https://de.wikipedia.org/wiki/Stromspiegel) bezeichnet man zwei Transistoren mit kurzgeschlossener Basis. Der obere Stromspiegel besteht aus zwei pnp-, der untere aus zwei npn-Transistoren. Ein Kurzschluss mit zwei baugleichen Widerständen, im unteren Teil der Abbildung sorgt dafür, dass die Emitter beider Transistoren des unteren Stromspiegels auf dem gleichen Potential gehalten werden, so dass durch beide Transistoren der gleiche Strom $I_{\mathrm{Sp}}$ abfließt. Aufgrund dieser Konstellation fließt der Differenzstrom
$$
\begin{equation*}
I_{\mathrm{diff}}=I_{2}-I_{3}
\end{equation*}
$$
(im Bild nach rechts) in die Verstärkerstufe ab. 

Nach diesem Prinzip übersetzt der Differenzverstärker bei intrinsich geringer Gleichtaktverstärkung kleine Spannungsdifferenzen in einen dazu proportionalen Strom $I_{\mathrm{diff}}$. 

Die Spannungen $U^{-}_{\mathrm{offset}},\ U^{+}_{\mathrm{offset}}$ an den Polen 1 und 5 in der Schaltung dienen zur Regulation für die weitere Gleichtaktunterdrückung, so dass sich Fertigungsunterschiede einzelner Bauelemente ausgleichen lassen.

#### Verstärkerstufe

Die **Verstärkerstufe** des $\mathrm{\mu A741}$ ist in **Abbildung 2** gezeigt: 

---

<img src="../figures/OPV_Verstaerkerstufe.png" width="350" style="zoom:100%;"/>

(**Abbildung 2**: Verstärkerstufe des $\mathrm{\mu A741}$)

---

Sie besteht aus einer [Darlington-Schaltung](https://de.wikipedia.org/wiki/Darlington-Schaltung), die $I_{\mathrm{diff}}$ aus der Eingangsstufe aufnimmt und in einen hohen Ausgangsstrom $I_{\mathrm{out}}^{T2}$ umsetzt. Die Schaltung entspricht dem Spezialfall eines [Emitterfolgers](https://de.wikipedia.org/wiki/Transistorgrundschaltungen#Emitterfolger), bei dem der Emitter des Transistors T1 die Basis des Transistors T2 ansteuert. Für den **Stromverstärkungsfaktor** $\beta_{\mathrm{DA}}$ gilt:
$$
\begin{equation*}
\begin{split}
&I_{\mathrm{out}}^{T2} = \beta_{T_{2}}\,I_{\mathrm{out}}^{T1};\qquad
I_{\mathrm{out}}^{T1} = \beta_{T_{1}}\,I_{\mathrm{diff}};\\
&\\
&\beta_{\mathrm{DA}} = \frac{I_{\mathrm{out}}^{T1}+I_{\mathrm{out}}^{T2}}{I_{\mathrm{diff}}} = \frac{I_{\mathrm{out}}^{T1}}{I_{\mathrm{diff}}}\,\left(1+\beta_{T_{2}}\right)\approx\beta_{T_{1}}\beta_{T_{2}},
\end{split}
\end{equation*}
$$
die Verstärkungsfaktoren von T1 und T2 multiplizieren sich also in erster Näherung. In der Praxis werden Kleinsignalverstärkungen von bis zu 50'000 erreicht. Über den Widerstand $R_{\mathrm{DA}}$ kann $I_{\mathrm{out}}^{T2}$ als Spannung abgegriffen werden. 

#### Ausgangsstufe

Als Spannungsabfall über $R_{\mathrm{DA}}$ hängt $U_{\mathrm{out}}^{\mathrm{DA}}$ noch stark von der angeschlossenen Last ab. Wäre dies bereits der Ausgang des OPV, dann würde die Spannungsverstärkung beim Anschluss einer bereits geringen Last stark abfallen. $U_{\mathrm{out}}^{\mathrm{DA}}$ wird daher an eine Ausgangsstufe, wie in **Abbildung 3** gezeigt, weitergeleitet: 

---

<img src="../figures/OPV_Ausgangsstufe.png" width="300" style="zoom:100%;"/>

(**Abbildung 3**: Ausgangsstufe des $\mathrm{\mu A741}$)

---

Die **Ausgangsstufe** besteht aus einer [Kollektorschaltung](https://de.wikipedia.org/wiki/Transistorgrundschaltungen#Kollektorschaltung_(Emitterfolger)), die keine eigene Spannungsverstärkung aufweist. Als [Impedanzwandler](https://de.wikipedia.org/wiki/Impedanzwandler) erfüllt sie den Zweck das Signal mit einem niedrigen Innenwiderstand auch für hohe Ströme stabil, als ideale Stromquelle, an den Verbraucher weiterzugeben. Nach innen weist sie einen hohen Widerstand auf, wodurch die Verstärkerstufe wiederum nicht belastet wird. 

Würde es sich um eine einfache Kollektorschaltung mit einem einzelnen Transistor handeln wäre die über den Emitterwiderstand $R_{E}$ permanent abfallende Leistung unwirtschaftlich hoch. Bei der hier verwendeten Schaltung handelt es sich um einen komplementären Emitterfolger ([Gegentaktendstufe](https://de.wikipedia.org/wiki/Gegentaktendstufe)), bei dem ein (oben) npn- und (unten) ein pnp-Transistor wechselseitig jeweils an eine Versorgungsspannung unterschiedlichen Vorzeichens angeschlossen sind. Bei einem positiven Signal ist der npn-Transistor offen und gibt das Signal weiter, während der pnp-Tansistor sperrt. Bei einem negativen Signal sind die Verhältnisse umgekehrt. 

Der Vorteil dieser Schaltung gegenüber der einfachen Kollektorschaltung besteht darin, dass im Arbeitspunkt $U_{\mathrm{out}}^{\mathrm{DA}}=0$ gewählt werden kann, so dass ohne Signal kein Ruhestrom fließt. Durch diese Wahl erreicht die Ausgangsstufe des OPV einen Wirkungsgrad von über 78%, gegenüber 6.5% bei einer einfachen Kollektorschaltung. Der Nachteil dieser Schaltung besteht darin, dass für jeden einzelnen Transistor erst ab einem Signal oberhalb der Diodenknickspannung $U_{D}$ ($|U_{\mathrm{out}}^{\mathrm{DA}}|\gtrsim |U_{D}|$) am Ausgang ein Strom ﬂießt. Die daraus resultierende Verzerrung für kleine Eingangssignale bezeichnet man als **Übernahmeverzerrung**. Um diese abzumildern weicht man von $U_{\mathrm{out}}^{\mathrm{DA}}=0$ als Arbeitspunkt ab und setzt beide Transistoren auf ein eigenes Potential mit der Differenz
$$
\begin{equation*}
|U_{\mathrm{out}}^{\mathrm{DA}} - U_{\mathrm{out}}^{\mathrm{DA}\prime}|=2\,U_{D}.
\end{equation*}
$$
Dies wird durch die Beschaltung mit weiteren Transistoren erreicht, die als Spannungsteiler mit z den Ausgangstransitoren identischen Eigenschaften fungieren.

Der Kondensator in der Mitte der Schaltung dient zur [Frequenzkompensation](https://de.wikipedia.org/wiki/Frequenzkompensation) und bestimmt $\nu_{\mathrm{G}}$.

## Essentials

Was Sie ab jetzt wissen sollten:

- Jeder OPV besteht aus einer **Eingangs-, Verstärker- und Ausgangsstufe**. 
- Die Aufgabe der Eingangstufe ist die **Differenzverstärkung bei möglichst hoher Gleichtsaktunterdrückung**. 
- Die Aufgabe der Ausgangsstufe ist die **Impedanzwandlung**, so dass die erreichte Verstärkung bis zur maximalen Last (durch $I_{a}^{\mathrm{max}}$), wie bei einer idealen Stromquelle, gleich bleibt. 

## Testfragen

1. Wie unterscheiden sich die Schaltsymbole eines npn- von einem pnp-Transistors?
2. Aus wie vielen Kondensatoren, Widerständen, npn- und pnp-Transistoren besteht der $\mathrm{\mu A741}$?
3. Wodurch zeichnet sich eine Kollektorschaltung aus?

# Navigation

[Main](https://gitlab.kit.edu/kit/etp-lehre/p2-praktikum/students/-/tree/main/Operationsverstaerker)

