# Technische Daten und Inventar für den Versuch **Operationsverstärker**

Für die verschiedenen Aufgaben des Versuchs **Operationsverstärker** stehen Ihnen die folgenden Geräte zur Verfügung:

- Ein Experimentiersteckbrett mit einem fest verbauten Transistor (vom Typ 2N2219A, npn) und 3 fest verbauten Operationsverstärkern (vom Typ LM741).
- Verschiedene Verbindungskabel, Dioden, Widerstände, und Kondensatoren (**verwenden Sie nötigenfalls benachbarte Werte**, falls Ihnen die in der Dokumentation verwendeten Werte nicht zur Verfügung stehen sollten). 
- Ein Frequenzgenerator ($0.2\ \mathrm{Hz} \ldots\ 2\ \mathrm{MHz}$; Sinus, Rechteck oder Dreieck; $0 \ldots \pm10\ \mathrm{V}$).
- Ein Oszilloskop (Tektronix , 2 Kanal).
- Ein Multimeter.

